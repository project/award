
Copyright 2008 Joshua Benner

Description
-----------
Award module allows administrators to identify one or more content types as
"Awards." When a user with appropriate permission views a user's profile, an
expandable fieldset is available where the viewing user may grant any awards
he has permission to grant.

Features
--------
* Specify multiple content types as awards
* Separate grant/receive permissions for each award content type
* Themable awards display

Prerequisites
-------------
* Drupal 5

Installation
------------
Copy the award folder to the appropriate modules folder, such as
sites/all/modules, then enable the module in Site Buildingi -> Modules.

Author
------
Joshua Benner (joshbenner /at\ gmail.com)

Thanks to my employer, Rock River Star (rockriverstar.com)